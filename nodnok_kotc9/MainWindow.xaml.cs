﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using nodnok_kotc9.Helper;

namespace nodnok_kotc9
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Values_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //проверка на целое число
            if (Regex.IsMatch(e.Text, "[^-0-9]"))
                e.Handled = true;

            //if (!char.IsDigit(e.Text,0)) 
            //    e.Handled = true;
        }

        private void ButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(InputValues.Text) || string.IsNullOrEmpty(InputCount.Text))
            {
                MessageBox.Show("Неверный ввод");
                return;
            }

            /*По условию необходимо: "Входные параметры: количество целых чисел для поиска НОД и НОК"
             * но использоваться не будет 
             */
            var count = Convert.ToInt32(InputCount.Text);
            
            var values = TextSplitter.Split(InputValues.Text);

            //проверка на введеные данные
            if (count != values.Length)
            {
                MessageBox.Show("Неверный ввод");
                return;
            } 

            var algorithm = new AlgorithmNodNok();

            Result.Text = $"НОД: {algorithm.FindGnd(values)}, НОК: {algorithm.FindLCM(values)}";
        }


        private void Count_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //проверка на натуральное число
            if (Regex.IsMatch(e.Text, "[^0-9]"))
                e.Handled = true;
        }
    }
}
