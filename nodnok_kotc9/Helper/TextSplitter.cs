﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nodnok_kotc9.Helper
{
    public class TextSplitter
    {
        public static int[] Split(string text)
        {
            var i = 0;
            var data = new List<int>();
            var words = text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var word in words)
            {
                data.Add(Convert.ToInt32(word));
                i++;
            }

            return Abs(data);
        }

        private static int[] Abs(IEnumerable<int> array)
        {
            return array.Select(Math.Abs).ToArray();
        }
    }
}
