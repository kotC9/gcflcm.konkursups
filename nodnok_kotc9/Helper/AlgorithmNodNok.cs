﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nodnok_kotc9.Helper
{
    public class AlgorithmNodNok
    {
        public int FindGnd(int[] values)
        {

            var a = GetGCD(values[0], values[1]);
            
            for (var i = 2; i < values.Length; i++)
                a = GetGCD(a, values[i]);
            
            return a;
        }

        public long FindLCM(int[] values)
        {
            // https://www.geeksforgeeks.org/lcm-of-given-array-elements/
            long lcm_of_array_elements = 1;
            var divisor = 2;

            while (true)
            {
                var counter = 0;
                var divisible = false;
                for (var i = 0; i < values.Length; i++)
                {

                    // lcm_of_array_elements (n1, n2, ... 0) = 0. 
                    // For negative number we convert into 
                    // positive and calculate lcm_of_array_elements. 
                    if (values[i] == 0)
                    {
                        return 0;
                    } 
                    if (values[i] < 0)
                    {
                        values[i] = values[i] * (-1);
                    }

                    if (values[i] == 1)
                    {
                        counter++;
                    }

                    if (values[i] % divisor == 0)
                    {
                        divisible = true;
                        values[i] = values[i] / divisor;
                    }
                }

                if (divisible)
                {
                    lcm_of_array_elements = lcm_of_array_elements * divisor;
                }
                else
                {
                    divisor++;
                }

                // Check if all element_array is 1 indicate  
                // we found all factors and terminate while loop. 
                if (counter == values.Length)
                {
                    return lcm_of_array_elements;
                }
            }
        }

        private int GetGCD(int x, int y)
        {
            while (y != 0)
            {
                var tmp = x % y;
                x = y;
                y = tmp;
            }
            return x;
        }

    }
}
